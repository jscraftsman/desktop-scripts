@ECHO OFF
REM Title: monitorKDS
REM Description: A batch script that will continuously check if KDService is running. This script will og if KDService is not running (C:\KDSLife.log)
REM Author: Gervel Giva

set /a INFO=03
set /a ERROR=04
COLOR %INFO%

ECHO This script will continuously check if KDService is running.
ECHO Ctrl+C to stop this script.
ECHO.

set /a c=1
set /a o=1
set /a f=0

:CHECK_KDS
TASKLIST | FIND "KDService.exe" >nul
IF ERRORLEVEL 1 (
	COLOR %ERROR%
	ECHO %date% %time% KDService is NOT running!
	SET /a f=f+1
	IF %f% LSS 5 (
		ECHO %date% %time% KDService has stopped working! >> C:\KDSLife.log
	)
) ELSE ( 
	COLOR %INFO%
	ECHO %date% %time% KDService is running...
	IF %f% GTR 5 (
		set /a f=0
	)
)


:CHECK_COUNT
TIMEOUT 3 >nul
set /a c=c+1
set /a o=o+1
IF %c% EQU 16 (
	CLS
	set /a c=1
	ECHO This script will continuously check if KDService is running.
	ECHO Ctrl+C to stop this script.
	ECHO.
)
GOTO CHECK_KDS
