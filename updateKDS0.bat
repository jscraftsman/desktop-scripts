@ECHO OFF
REM Title: updateKDS
REM Description: A batch file that will update the KDService 1.0 file [Release Version].
REM Author: Gervel Giva

REM This script will update the KDService 1.0 with a release version.

REM Get Latest KDService 1.0
IF EXIST "E:\P4 Workstations\depot\Projects\KMC\KDService\KDService\bin\Release\KDService.exe" (
	ECHO Replacing KDService 1.0 common copy.
	XCOPY /Y "E:\P4 Workstations\depot\Projects\KMC\KDService\KDService\bin\Release\KDService.exe" \\10.191.21.45\"Installers\KDSERVICE - CLIENT SERVER BUILDS\KDService\1.0\Release\KDService.exe"
)

:CHECK_KDS
TASKLIST | FIND "KDService.exe" >nul
IF ERRORLEVEL 1 (
  GOTO KDS_STOPPED
) ELSE (
  ECHO KDService is still running.. Stopping KDService...
  sc stop kdservice
  timeout 3 >nul
  GOTO CHECK_KDS
)

:KDS_STOPPED
ECHO Updating KDService...
XCOPY /Y \\10.191.21.45\"Installers\KDSERVICE - CLIENT SERVER BUILDS\KDService\1.0\Release\KDService.exe" "C:\Program Files\KDService\bin\KDService.exe"
IF ERRORLEVEL 1 (
  echo Failed to update KDServicce...
  GOTO CHECK_KDS
) ELSE (
  echo KDService updated...
  GOTO LOG_REMOVED
)

:LOG_REMOVED
ECHO Starting KDService...
sc start kdservice
