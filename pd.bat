@ECHO OFF 
REM Title: PD (Project Directory)
REM Description: A batch script that will act as a shortcat for changing directories
REM Author: nameless

ECHO Select a directory:
ECHO [1] Project Alpha (E:\Nodejs Projects\project-alpha)
SET ONE="E:\Nodejs Projects\project-alpha"

ECHO [2] Spooler (C:\Windows\System32\)
SET TWO="C:\Windows\System32\"

ECHO [3] Printer Driver folder (C:\Windows\System32\spool\drivers\x64\3\)
SET THREE="C:\Windows\System32\spool\drivers\x64\3\"

ECHO.
SET /P directory="Select directory: "
ECHO.

IF %directory% EQU 1 (
    ECHO Changing path to %ONE%
    CD /D %ONE%
    GOTO END
)

IF %directory% EQU 2 (
    ECHO Changing path to %TWO%
    CD /D %TWO%
    GOTO END
)

IF %directory% EQU 3 (
    ECHO Changing path to %THREE%
    CD /D %THREE%
    GOTO END
)

CLS
ECHO Incorrect input!
ECHO Input: %directory%

:END
CLS
ECHO Directory Changed.

