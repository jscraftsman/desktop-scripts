@ECHO OFF 
REM Title: versionKLS 
REM Description: A batch script that will check the current version of KDS (KDService), LM (Language Monitor), and SM (Status Monitor)
REM Author: Gervel Giva

ECHO KDService
WMIC datafile where Name="C:\\Program Files\\KDService\\bin\\KDService.exe" get Version
REM KDService Path: [C:\Program Files\KDService\bin\KDService.exe]

ECHO Status Monitor
IF EXIST "C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" (
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" get Version
	REM Status Monitor Path: [C:\Windows\System32\spool\drivers\x64\3\KDSSTM.exe]
) ELSE (
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KDSSTM.exe" get Version
	REM Status Monitor Path: [C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.exe]
)

ECHO %PROCESSOR_ARCHITECTURE% | FIND /i "x86" > nul
IF %ERRORLEVEL%==0 (
	ECHO Language Monitor [32-bit \Windows\System32]
	WMIC datafile where Name="C:\\Windows\\System32\\KXPLM32.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\KXPLM32.dll]
	
	ECHO.
	
	ECHO Language Monitor [32-bit \Windows\System32\spool\drivers\W32X86\3]
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KXPLM32.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\spool\drivers\W32X86\3\KXPLM32.dll]
) ELSE (
	ECHO Language Monitor [64-bit \Windows\System32]
	WMIC datafile where Name="C:\\Windows\\System32\\KXPLM64.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\KXPLM64.dll]
	
	ECHO.
	
	ECHO Language Monitor [64-bit \Windows\System32\spool\drivers\x64\3]
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KXPLM64.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\spool\drivers\x64\3\KXPLM64.dll]
)

PAUSE