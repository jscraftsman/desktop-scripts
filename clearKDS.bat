@ECHO OFF
::REM Title: clearKDS
::REM Description: A batch file that will stop kdservice, delete the KDS log file then restart the kdservice
::REM Author: Gervel Giva

ECHO This script will clear the KDS Log...

:CHECK_KDS
TASKLIST | FIND "KDService.exe" >nul
IF ERRORLEVEL 1 (
  GOTO KDS_STOPPED
) ELSE (
  ECHO KDService is still running.. stopping KDService...
  sc stop kdservice
  timeout 3 >nul
  GOTO CHECK_KDS
)

:KDS_STOPPED
ECHO Removing KDService log file
del "C:\ProgramData\KDService\KDService.log"
IF ERRORLEVEL 1 (
  ECHO Failed to remove log file...
  GOTO CHECK_KDS
) ELSE (
  ECHO Log file removed...
  GOTO LOG_REMOVED
)

:LOG_REMOVED
ECHO Restarting KDService...
sc start kdservice
