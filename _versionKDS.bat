@ECHO OFF 
REM Title: versionKDS 
REM Description: A batch script that will check the current version of SM, KDS, and LM
REM Author: Gervel Giva

ECHO KDService
WMIC datafile where Name="C:\\Program Files\\KDService\\bin\\KDService.exe" get Version

ECHO Status Monitor
IF EXIST "C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" (
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" get Version
) ELSE (
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KDSSTM.exe" get Version
)

ECHO %PROCESSOR_ARCHITECTURE% | FIND /i "x86" > nul
IF %ERRORLEVEL%==0 (
	ECHO Language Monitor [32-bit]
	WMIC datafile where Name="C:\\Windows\\System32\\KXPLM32.dll" get Version
) ELSE (
	ECHO Language Monitor [64-bit]
	WMIC datafile where Name="C:\\Windows\\System32\\KXPLM64.dll" get Version
	ECHO.
	ECHO Language Monitor [64-bit Spooler]
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KXPLM64.dll" get Version
	
)

TIMEOUT 5