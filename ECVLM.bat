@ECHO OFF 
REM Title: ECVLM
REM Description: Expand and Check LM version of the current directory
REM Author: Gervel Giva
REM Version: 1.0

REM Syntax CheckLMUKX

REM EXAMPLE: ECVLM 

REM Notes: Must be in the directory

ECHO Checking the LM version of this directory

SET DRIVER_BASE=%1
ECHO.

REM ############# 32bit #############
IF EXIST kxplm32.dl_ (
	EXPAND kxplm32.dl_ kxplm32.dll > NUL
	ECHO.
	GOTO Expand32
) ELSE (
	ECHO kxplm32.dl_ is not present in this directory
	GOTO Check64
)

REM ############# 64bit #############
:Check64
IF EXIST kxplm64.dl_ (
	EXPAND kxplm64.dl_ kxplm64.dll > NUL
	ECHO.
	GOTO Expand64
) ELSE (
	ECHO kxplm64.dl_ is not present in this directory
	GOTO Done
)

:Expand32
	IF EXIST kxplm32.dll (
		ECHO Version of: kxplm32.dll
		CALL GetVersion "%cd%\kxplm32.dll"
		GOTO Done
	) ELSE (
		ECHO kxplm32.dll is not present in this directory
		GOTO Done
	)


:Expand64
	IF EXIST kxplm64.dll (
		ECHO Version of: kxplm64.dll
		CALL GetVersion "%cd%\kxplm64.dll"
		GOTO Done
	) ELSE (
		ECHO kxplm64.dll is not present in this directory
		GOTO Done
	)

:Done
	ECHO.
	ECHO Task complete.