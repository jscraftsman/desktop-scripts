var VER = "1.0.13";

var counter_r = 0;
var checker;
var updateScheduleTimer;

var STORAGE_KEY_RESERVER = "run_reserver";
var STORAGE_KEY_RELOAD = "run_reserver_reload_complete";
var STORAGE_KEY_SCHEDULE = "schedule";

var SCHEDULE_TEXT_ID = "ctl00_ctl33_g_014819a3_2b25_4776_a6b7_5accde785fb5_schedule";
var RESERVE_BTN_ID = "ctl00_ctl33_g_014819a3_2b25_4776_a6b7_5accde785fb5_btnReserved";
var SCHEDULE_ID = "ctl00_ctl33_g_014819a3_2b25_4776_a6b7_5accde785fb5_ddSchedule";
var MSG_ID = "ctl00_ctl33_g_014819a3_2b25_4776_a6b7_5accde785fb5_lblMessage";
var SELECTED_SCHEDULE = "";

// DEBUG CONTROLS
var CS;
var SR;
var GT;

$(function() {
    console.log("[GRS Auto Reserve Script] Version: " + VER);

    addReserverUI();
    loadSettings();
    $("#" + SCHEDULE_TEXT_ID).on("click", "#reserver", handleReserverToggle);

    ///////////////////////////////////
    //////////// Functions ////////////
    ///////////////////////////////////

    function addReserverUI() {
        $("#" + SCHEDULE_TEXT_ID).append("[<input type='checkbox' id='reserver'>Togge Reserver <<span id='counter_r'></span>> ]");
        $("#" + SCHEDULE_TEXT_ID).append("<input type='text' id='scheduleText' placeholder='Schedule' />");
    }

    function handleReserverToggle(e) {
        var r = $("#reserver")[0];
        if (r.checked) {
            SELECTED_SCHEDULE = $("#" + SCHEDULE_ID).val();
            $("#scheduleText").val(SELECTED_SCHEDULE);
            if (SELECTED_SCHEDULE === "[PLEASE SELECT SCHEDULE]") {
                $("#reserver")[0].checked = false;
                $("#scheduleText").val("");
                alert("Select a schedule!");
                return;
            } else {
                console.log("handleReserverToggle() - Selected schedule: " + SELECTED_SCHEDULE);
                console.log("handleReserverToggle() - Starting Checker()...");

                counter_r = 0;
                checker = setInterval(startChecker, 1000);

                localStorage[STORAGE_KEY_RESERVER] = true;
                localStorage[STORAGE_KEY_SCHEDULE] = SELECTED_SCHEDULE;
            }

        } else {
            $("#scheduleText").val("");
            stopReserver();
            stopChecker();
        }
    }

    function startChecker() {
        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();

        $("#" + SCHEDULE_ID + " option[value='[PLEASE SELECT SCHEDULE]'").attr('selected', null);
        $("#" + SCHEDULE_ID + " option[value='" + SELECTED_SCHEDULE + "'").attr('selected', 'selected');

        if (h >= 12 && m >= 45) {
            if (localStorage) {
                if (localStorage[STORAGE_KEY_RELOAD]) {
                    if ($("#" + RESERVE_BTN_ID).attr("disabled") !== "disabled") {
                        callReserver("startChecker() - ");
                    } else {
                        ChangeSchedule();
                    }
                } else {
                    ChangeSchedule();
                }
            }
        } else {
            counter_r++;
            var loadingStr = ((counter_r % 3 === 0) ? "-  " : ((counter_r % 3 === 1) ? "-- " : "---"));
            $("#counter_r").text(loadingStr);
        }
    }

    function ChangeSchedule() {
        console.log("ChangeSchedule() - Updating schedule to " + SELECTED_SCHEDULE + " ...");
        var msg = $("#ctl00_ctl33_g_014819a3_2b25_4776_a6b7_5accde785fb5_lblMessage").text();

        if (msg === "NOTE: No more waiting slots available on this date") {
            stopChecker("ChangeSchedule() - ");
            stopReserver();
            $("#reserver")[0].checked = false;
            $("#scheduleText").val("");
            alert("No more available slots!");
        } else {
            updateScheduleTimer = setTimeout(applySchedule, 500);
        }
    }

    function applySchedule() {
        $("#" + SCHEDULE_ID + " option[value='[PLEASE SELECT SCHEDULE]'").attr('selected', null);
        $("#" + SCHEDULE_ID + " option[value='" + SELECTED_SCHEDULE + "'").attr('selected', 'selected');

        localStorage[STORAGE_KEY_RELOAD] = true;
        __doPostBack('ctl00$ctl33$g_014819a3_2b25_4776_a6b7_5accde785fb5$ddSchedule', '');
    }

    function Reserver() {
        stopReserver();
        $("#" + RESERVE_BTN_ID).click();
    }

    function stopReserver() {
        console.log("stopReserver() - Stoping Reserver()...");
        clearInterval(updateScheduleTimer);

        $("#counter_r").text("");
        delete localStorage[STORAGE_KEY_SCHEDULE];
        delete localStorage[STORAGE_KEY_RESERVER];
    }

    function stopChecker(caller) {
        console.log(caller + " stopChecker() - Stop Checker.");
        clearInterval(checker);
    }

    function loadSettings() {
        if (localStorage) {
            if (localStorage[STORAGE_KEY_RESERVER]) {
                if (localStorage[STORAGE_KEY_RESERVER] == "false") return;

                var r = $("#reserver")[0];
                r.checked = localStorage[STORAGE_KEY_RESERVER];

                if (r.checked) {
                    if (localStorage[STORAGE_KEY_SCHEDULE] === undefined) {
                        console.log("loadSettings() - STORAGE_KEY_SCHEDULE has no value...");
                    }

                    SELECTED_SCHEDULE = localStorage[STORAGE_KEY_SCHEDULE];
                    $("#scheduleText").val(SELECTED_SCHEDULE);

                    if ($("#" + RESERVE_BTN_ID).attr("disabled") !== "disabled") {
                        callReserver("loadSettings() - ");
                    } else {
                        checker = setInterval(startChecker, 1000);
                    }
                }
            } else {
                console.log("loadSettings() - STORAGE_KEY_RESERVER is not defined.");
            }
        } else {
            console.log("loadSetting() - localStorage error");
        }
    }

    function callReserver(caller) {
        stopChecker(caller);

        delete localStorage[STORAGE_KEY_RELOAD];
        delete localStorage[STORAGE_KEY_RESERVER];

        console.log("callReserver() - Calling Reserver()...");
        Reserver();
    }

    function getTimer() {
        return counter_r;
    }

    CS = ChangeSchedule;
    SR = stopReserver;
    GT = getTimer;
});