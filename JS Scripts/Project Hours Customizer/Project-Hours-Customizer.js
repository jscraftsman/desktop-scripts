$(() => {
    const VERSION = '1.0.0';
    const CDN_SELECTIZE_CSS = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css';
    const CDN_SELECTIZE_JS = 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js';

    if (isProjectHours()) {
        console.log('[START] Attempting to add Selectize...');

        addSelectizeCSS();
        addSelectizeJS();

        console.log(`[INFO] Script version ${VERSION}`);
    }

    function addSelectizeCSS() {
        $("<link/>", {
            rel: "stylesheet",
            type: "text/css",
            href: CDN_SELECTIZE_CSS
        }).appendTo("head");
    }

    function addSelectizeJS() {
        $.getScript(CDN_SELECTIZE_JS).done(selectizeJS_Added).fail();
    }

    function selectizeJS_Added(script, textStatus) {
        $('select').selectize();
        console.log('[INFO] Selectize successfully added.');
    }

    function selictizeJS_Failed(jqxhr, settings, exception) {
        console.log('[ERROR] Failed to add Selectize script...');
        console.log(exception);
    }

    function isProjectHours() {
        return location.href.indexOf("http://kddpsvrspprod2/sites/ProjectHours/Lists/Project%20Hours%20V2/NewForm.aspx") !== -1;
    }
});