/*
	Title: addCustomToggleEvent
	Description: A custom Javascript library that will hide "romaji" column and "meaning" column from the page: http://nihongoichiban.com/2011/04/30/complete-list-of-vocabulary-for-the-jlpt-n5/ if the cursor is away from the cell,	then show the info on mouse over.
			
	Author: Gervel Giva (gervelgiva@gmail.com)
	Usage:	
		addCustomToggleEvent()
*/
function addCustomToggleEvent(){
	var td = document.querySelectorAll("td")
		for(t in td){
			if(isNaN(t)) {
				continue;
			}
			if(td[t].innerHTML == "<strong>Kanji</strong>" || td[t].innerHTML == "Kanji") continue;
			if( (td[t].innerHTML).indexOf("Furigana") != -1) continue;
			if(td[t].innerHTML == "<strong>Romaji</strong>" || td[t].innerHTML == "Romaji") continue;
			if(td[t].innerHTML == "<strong>Meaning</strong>" || td[t].innerHTML == "Meaning") continue;
			
			var o = td[t].getAttribute('bgcolor');
			if(o == "#ffe9e5"){    
				td[t].setAttribute('bgcolor', "#ffff00");
			}
	}

	var romaji = document.querySelectorAll("tr td:nth-child(3)");
	var meaning = document.querySelectorAll("tr td:nth-child(4)");
	setupMouseEvent(romaji, true);
	setupMouseEvent(meaning, false);
}

function setupMouseEvent(cell, assign){
	Window.Cache = {};
	for(var i in cell){
		if(i == 0){
			continue;
		}
		if(cell[i]){
			if(cell[i]["style"]){
				
				if(i == "length" || i == "size") continue;
				if(cell[i].innerHTML == "<strong>Romaji</strong>" || cell[i].innerHTML == "Romaji") continue;
				if(cell[i].innerHTML == "<strong>Meaning</strong>" || cell[i].innerHTML == "Meaning") continue;
				
				cell[i]["clicked"] = false;
				cell[i]["style"]["color"] = "#fff";   
				
				cell[i].onmouseover = function(){
					this["style"]["color"] = "#333";
				};
				
				cell[i].onmouseout = function(){
					if(this["clicked"] == false){
						this["style"]["color"] = "#fff";
					}							
				};
				
				cell[i].onclick = function(e){
					e.preventDefault();
					this["clicked"] = !this["clicked"];
					if(this["clicked"] == false){
						this["style"]["color"] = "#fff";
					}else{
						this["style"]["color"] = "#333";
					}
				}
			}else{
				continue;
			}
		}else{
			continue;
		}
	}
}

addCustomToggleEvent();
