$(function() {
    var REV_COUNT = 0;
    var REV_TARGET = 40;

    var TIMER;

    if (location.pathname === "/review/session") {
        TIMER = setInterval(wrapUp, 1000); // [TODO] Explain why this is set in an interval
    }

    function wrapUp() {
        REV_COUNT = parseInt($('#completed-count').text());
        if (REV_COUNT === REV_TARGET) {
            if (!$('#option-wrap-up').hasClass('wrap-up-selected')) {
                $('#option-wrap-up').click();
                clearInterval(TIMER);
            }
        }
    }

});