$(function() {
    if (location.pathname === "/review/session") {
        document.addEventListener('copy', addBinding);
    }

    function addBinding(e) {
        if (window.getSelection().isCollapsed) {
            var current = document.querySelector("#character span").innerText;
            if (current) {
                if (document.querySelector("#character").classList.contains("vocabulary")) {
                    window.open("https://www.wanikani.com/vocabulary/" + current);
                } else if (document.querySelector("#character").classList.contains("kanji")) {
                    window.open("https://www.wanikani.com/kanji/" + current);
                } else {
                    console.log("Not a kanji or vocabulary...");
                    console.log(document.querySelector("#character").classList);
                }
            }
            e.preventDefault();
        }
    }
});