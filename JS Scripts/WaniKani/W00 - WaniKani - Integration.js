$(function() {
    var REV_COUNT = 0;
    var REV_TARGET = 40;
    var TIMER;

    if (location.pathname === "/review/session") {
        document.addEventListener('copy', addBinding);
        TIMER = setInterval(wrapUp, 1000); // [TODO] Explain why this is set in an interval
    }

    function wrapUp() {
        REV_COUNT = parseInt($('#completed-count').text());
        if (REV_COUNT === REV_TARGET) {
            if (!$('#option-wrap-up').hasClass('wrap-up-selected')) {
                $('#option-wrap-up').click();
                clearInterval(TIMER);
            }
        }
    }

    function addBinding(e) {
        if (window.getSelection().isCollapsed) {
            var current = document.querySelector("#character span").innerText;
            if (current) {
                if (document.querySelector("#character").classList.contains("vocabulary")) {
                    window.open("https://www.wanikani.com/vocabulary/" + current);
                } else if (document.querySelector("#character").classList.contains("kanji")) {
                    window.open("https://www.wanikani.com/kanji/" + current);
                } else {
                    console.log("Not a kanji or vocabulary...");
                    console.log(document.querySelector("#character").classList);
                }
            }
            e.preventDefault();
        }
    }
});