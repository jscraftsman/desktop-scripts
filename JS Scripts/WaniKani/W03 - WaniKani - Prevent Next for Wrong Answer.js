/* jshint esversion: 6 */

/*
    Script that will prevent the key 'enter' if previous items's answer was incorrect
*/

var LAST_INPUT_INCORRECT = false;

$(function() {
    'use strict';

    if (location.pathname === "/review/session") {
        let inlineBinding = `javascript: preventNext(event); return void(0)`;
        $("#answer-form button").attr('onclick', inlineBinding);
    }
});

function preventNext(event) {
    console.log('preventNext() called');
    console.log('event', event);

    if (LAST_INPUT_INCORRECT === true) {
        event.stopImmediatePropagation();

        LAST_INPUT_INCORRECT = false;
        alert('Stop');

        // NOT WORKING... the UI will still change (next item is shown)
    }

    let inputDisabled = $("#user-response").prop('disabled');

    if (inputDisabled) {
        LAST_INPUT_INCORRECT = true;
    }

    n = function() {
        return $("#answer-exception").remove(),
            e.nextQuestion(),
            additionalContent.closeItemInfo(), !1
    }
}