const RETRY_DELAY = 1000;
let EVENT_ADDED = false;

$(() => {
	let attemptCount = 0;

	addSelectAllEvent();

    function addSelectAllEvent() {
        console.log("attempting to add selectAllHander()");
        if(canAddHandler()) {
            EVENT_ADDED = true;
            hookEvent();
			console.log("successfully added selectAllHander()");
        } else {
			attemptCount++;
			console.log(`failed... retrying in ${RETRY_DELAY} milliseconds. Attempts: ${attemptCount}`);
            setTimeout(addSelectAllEvent, RETRY_DELAY);
        }
    }

    function hookEvent() {
    	$(".v-section").on("click", ".container-tasks-count", selectAllHander);
    }

    function selectAllHander(e) {
    	$(e.target).parents(".v-section").find(".js-select-task").click(); 
    }

	function canAddHandler() {
		return ($(".v-section").length > 0);
	}
});

