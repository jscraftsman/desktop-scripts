Version: v1.0.1

= Change log =
- v0.0.1 -
	- Initial release (Alpha)
	- Added
- v1.0.0 -
	- Revamped the UI of the System.
	- [Bug Fix] If employee has timed-in above 12:45 PM; lunch break is included in the calculation.
- v1.0.1 -
	- Updated mapping to css stylesheet

======================================

= Required Browser = 
- Google Chrome

= Installation Guide (Chrome - One Time Only) =
1. Install CJS (Custom Javascript for Websites) chrome plugin
    > https://chrome.google.com/webstore/detail/custom-javascript-for-web/poakhlngfciodnhlhhgnaaelnpjljija
    > A "cjs" icon should appear beside the address bar (right side of the star)
2. Once the CJS plugin is installed, go to the Electronic time Keeping (Actatek) application and logged in.
    > http://kddpsvrams/timekeeping/WebLogin.aspx
3. Open 'tktac.js' using any text editor and copy all the contents of the file.
4. In the browser, click the 'cjs' icon and replace '// Here You can type your custom JavaScript...' with the contents of 'tktac.js' and click 'save'
-> Make sure that "enable cjs for this host" is checked.

= Upgrading the Script =
1. Obtain a copy of the updated 'tktac.js'
2. Open 'tktac.js' using any text editor and copy all the contents of the file.
3. In the browser, click the 'cjs' icon and replace all the text with the contents of the new 'tktac.js' and click 'save'

= Disabling the Script =
1. Go to http://kddpsvrams/timekeeping/WebLogin.aspx, and click the 'cjs' icon
2. Remove all text content and click save.

======================================

= Known Limitations =
- If a record has more than 5 Time IN/OUT data, the table is no longer pleasing to look at.
- Invalid dates are not rendered (just like in the base system)
- UI for login and change password was not revamped.

= Notes =
- If you find any bugs or incorrect calculations please send me a (1)screenshot of the page and (2) description of the issue.
- Refer to "Calculation Guide.JPG" for the calculation used by the system.
