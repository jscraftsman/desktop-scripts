/* jshint esversion: 6 */

$(function() {
    'use script';

    var VERSION = '1.0.1';

    const TIMETABLE_ROW_ID = 'objwdw_attList_detail_';

    var TH_COUNT = 1;

    var EMPTY_RECORD = {
        DOW: null,
        DATE: null,
        SHIFT: null,
        TARDY: null,
        UTIME: null, // Undertime
        THOURS: null, // Total Hours
        REGWORK: null,
        EHOURS: null, // Excess Hours
        INS: [],
        OUTS: [],
        IS_WEEKEND: false,
        IS_HOLIDAY: false
    };

    var RECORD_QUERY = {
        DOW: 'compute_1',
        DATE: 'daily_date',
        SHIFT: 'shift_code',
        TARDY: 'compute_6',
        UTIME: 'compute_5', // Undertime
        THOURS: 'compute_2', // Total Hours
        REGWORK: 'compute_3',
        EHOURS: 'compute_4', // Excess Hours
        INS: 'in',
        OUTS: 'out'
    };

    var LEGEND = {
        LATE: 'danger',
        REST_DAY: 'active',
        REST_DAY_LABEL: 'default',
        INCOMPLETE: 'warning'
    }


    const CSS_SOURCES = [
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
		'https://bootswatch.com/3/flatly/bootstrap.min.css'
    ];

    const JS_SOURCES = [
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'
    ];

    const BODY = `


<style type="text/css">
    /* If you want you can use font-face */
    
    .clock {
        width: 800px;
        margin: 0 auto;
        padding: 30px;
        color: #2c3e50;
    }
    
    #Date {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 36px;
        text-align: center;
        text-shadow: 0 0 5px #2c3e50;
    }
    
    #clock-container ul {
        width: 800px;
        margin: 0 auto;
        padding: 0px;
        list-style: none;
        text-align: center;
    }
    
    #clock-container ul li {
        display: inline;
        font-size: 10em;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        text-shadow: 0 0 5px #2c3e50;
    }
    
    #point {
        position: relative;
        -moz-animation: mymove 1s ease infinite;
        -webkit-animation: mymove 1s ease infinite;
        padding-left: 10px;
        padding-right: 10px;
    }
    /* Simple Animation */
    
    @-webkit-keyframes mymove {
        0% {
            opacity: 1.0;
            text-shadow: 0 0 20px #2c3e50;
        }
        50% {
            opacity: 0;
            text-shadow: none;
        }
        100% {
            opacity: 1.0;
            text-shadow: 0 0 20px #2c3e50;
        }
    }
    
    @-moz-keyframes mymove {
        0% {
            opacity: 1.0;
            text-shadow: 0 0 20px #2c3e50;
        }
        50% {
            opacity: 0;
            text-shadow: none;
        }
        100% {
            opacity: 1.0;
            text-shadow: 0 0 20px #2c3e50;
        }
        ;
    }
    
    #timetable tr,
    #timetable th {
        text-align: center;
    }
    
    #legend {
        margin-top: 0.5em;
    }
</style>

<div class="navbar navbar-default navbar-fixed-top">
    <div class='container'>
        <nav>
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#edp-nav-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand static-only" id="emp-name" href="#">Employee</a>
                </div>

                <div class="collapse navbar-collapse" id="edp-nav-bar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle static-only" data-toggle="dropdown" role="button" aria-expanded="false">Options <span class="glyphicon glyphicon-cog"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(" LinkButtonPassword ", " ", false, " ", "../WebChangePass.aspx ", false, true))'>Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("LinkButtonOut", "", false, "", "../Logout.aspx", false, true))'>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class='container'>

    <div>
        <br /> <br /> <br />

        <div id="clock-container" class="clock">
            <div id="Date"></div>
            <ul>
                <li id="hours"></li>
                <li id="point">:</li>
                <li id="min"></li>
                <li id="point">:</li>
                <li id="sec"></li>
            </ul>
        </div>

        <h1 class="text-center"> Estimated TimeOut: <strong><span id="cto"></strong></h1>

        <hr />

        <div id="date-range-container row">
            <div class="col-lg-3">
                <input type="date" class="form-control" id="date-range-from" value="2017-01-01" />
            </div>
            <div class="col-lg-3">
                <input type="date" class="form-control" id="date-range-to" value="2017-01-30" />
            </div>
            <div class="col-lg-2">
                <button id="retrieve" class="btn btn-primary">Retrieve</button>
            </div>
            <div id="legend" class="col-lg-offset-1 col-lg-3">
                <p>LEGEND: <span class="label label-${LEGEND.LATE}">LATE</span> <span class="label label-${LEGEND.REST_DAY_LABEL}">REST DAY</span> <span class="label label-${LEGEND.INCOMPLETE}">INCOMPLETE</span></p>
            </div>
        </div>

        <table id="timetable" class="table table-striped table-hover ">
            <thead>
                <tr>
                    <th>Day</th>
                    <th>Date</th>
                    <th>Shift</th>
                    <th>Tardy</th>
                    <th>U-Time</th>
                    <th>Total Hours</th>
                    <th>Reg Work</th>
                    <th>Excess Hours</th>
                    <th>Time In/Out</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>

        <hr />

        <footer class="text-center">Electronic time Keeping - Actatek | Customized by <i>nameless</i> &#169; 2017 v${VERSION} </footer>
    </div>
</div>




    `;

    var DATA = {};

    // 1. Neccessary files. Wait for this operations to be completed before proceeding...
    addCSS();
    addJS();

    // 2. Extract neccessary data
    extractData();

    // 3. Clean Dom
    $('form#form1').hide();
    

    // 4. Render new layout
    $('body').append(BODY);
    renderLayout();

    $('body').on('click', '#retrieve', dateRangeHandler);

    // DEBUG
    console.log(`EDP v${VERSION}`);

    function addCSS() {
        var link;

        CSS_SOURCES.forEach((href) => {
            link = document.createElement('link');
            link.type = 'text/css';
            link.rel = 'stylesheet';
            link.href = href;
            document.head.appendChild(link);
        });
    }

    function addJS() {
        var script;

        JS_SOURCES.forEach((src) => {
            script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = src;
            document.body.appendChild(script);
        });
    }

    function extractData() {
        DATA.name = $('#LabelEmp').text();
        DATA.TIMETABLE = FetchData();
        DATA.ETO = CalculateTimeout();
        DATA.From = JSON.parse($('#RadDateFrom_dateInput_ClientState').val());
        DATA.To = JSON.parse($('#RadDateTo_dateInput_ClientState').val());
    }

    function renderLayout() {
        $('nav a.static-only').hover((e) => {
            $('nav a.static-only').css('color', 'white');
            $('nav a.static-only').css('cursor', 'default');
        });
        $('#emp-name').text(DATA.name || 'Employee');
        CreateClock();
        $('#cto').text(DATA.ETO);
        RenderTimeTable();
        renderRange();
    }

    function CreateClock() {
        // Create two variable with the names of the months and days in an array
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

        // Create a newDate() object
        var newDate = new Date();
        // Extract the current date from Date object
        newDate.setDate(newDate.getDate());
        // Output the day, date, month and year   
        $('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

        setInterval(function() {
            // Create a newDate() object and extract the seconds of the current time on the visitor's
            var seconds = new Date().getSeconds();
            // Add a leading zero to seconds value
            $("#sec").html((seconds < 10 ? "0" : "") + seconds);
        }, 1000);

        setInterval(function() {
            // Create a newDate() object and extract the minutes of the current time on the visitor's
            var minutes = new Date().getMinutes();
            // Add a leading zero to the minutes value
            $("#min").html((minutes < 10 ? "0" : "") + minutes);
        }, 1000);

        setInterval(function() {
            // Create a newDate() object and extract the hours of the current time on the visitor's
            var hours = new Date().getHours();
            // Add a leading zero to the hours value
            $("#hours").html((hours < 10 ? "0" : "") + hours);
        }, 1000);
    }

    function CalculateTimeout() {
        var i = 0;
        var timeIN = [];
        var timeOUT = [];

        // Get current date for server
        var dateStr = $('#Label1').text().replace('|', '');
        var currentDate = new Date(dateStr);

        var CURRENT_DATE;
        var CURRENT_DATE_INDEX = -1;

        // Find current date. Extract index of DOM of current date.
        var dates = $('span[onclick*="daily_date"]');
        var today = currentDate.format('MM/dd/yyyy');
        for (i = 0; i < dates.length; i++) {
            if ($(dates[i]).text() === today) {
                CURRENT_DATE_INDEX = i;
                break;
            }
        }
        
        if (CURRENT_DATE_INDEX == -1) {
            if(localStorage["edp_today"]) {
                CURRENT_DATE = localStorage["edp_today"];
                console.log(`Estimated time out is for: ${CURRENT_DATE}`);
            } else {
                throw 'Current date not found';
            }
        } else {
            // Save current date
            localStorage["edp_today"] = today;
        }

        // Extract time in and time out datas
        var timeStr;
        var inContainers = $(dates[CURRENT_DATE_INDEX]).parent().find('span[onclick*="in"]');
        if (inContainers.length === 0) {
            timeIN = localStorage["edp_time_ins"].split(',');
        } else {
            for (i = 0; i < inContainers.length; i++) {
                timeStr = $(inContainers[i]).text().trim();
                if (timeStr !== '' && timeStr.length < 6) {
                    timeIN.push(timeStr);
                }
            }

            if (timeIN.length === 0) {
                throw 'No TIME IN data found';
            } else {
                // Save timeIN array
                localStorage["edp_time_ins"] = timeIN.join(',');
            }
        }
        

        var outContainers = $(dates[CURRENT_DATE_INDEX]).parent().find('span[onclick*="out"]');
        if (inContainers.length === 0) {
            timeOUT = localStorage["edp_time_outs"].split(',');
        } else {
            for (i = 0; i < outContainers.length; i++) {
                timeStr = $(outContainers[i]).text().trim();
                if (timeStr !== '' && timeStr.length < 6) {
                    timeOUT.push(timeStr);
                }
            }

            // Save timeOUT array
            localStorage["edp_time_outs"] = timeOUT.join(',');
        }


        // HANDLE SHIFT
        var SHIFT = parseInt($(dates[CURRENT_DATE_INDEX]).parent().find('span[onclick*="shift_code"]').text());
        if (isNaN(SHIFT)) {
            if(localStorage["edp_shift"]) {
                SHIFT = localStorage["edp_shift"];
            } else {
                throw 'Shift is invalid';
            }
        } else {
            localStorage["edp_shift"] = SHIFT;
        }

        // Get First Time In.
        // Calculate Estimated Time out (9 hours)
        var _timeIN = timeIN[0].split(':');

        // Handle shift buffer
        var _timeIN_MIN_SHIFT = ((parseInt(_timeIN[0]) * 60) + parseInt(_timeIN[1]));
        _timeIN_MIN_SHIFT = (_timeIN_MIN_SHIFT < ((SHIFT * 60) - 60) ? ((SHIFT * 60) - 60) : _timeIN_MIN_SHIFT);

        var ESTIMATED_TIME_OUT = {
            hour: 0,
            min: 0
        };
        
        const ELEVEN_FORTY_FIVE = 705; // ((11 * 60) + 45
        const TWELVE_FORY_FIVE = 765 // ((12 * 60) + 45
        var REQ_HOURS = (_timeIN_MIN_SHIFT < ELEVEN_FORTY_FIVE ? 9 : 8);
        if(_timeIN_MIN_SHIFT >= ELEVEN_FORTY_FIVE && _timeIN_MIN_SHIFT <= TWELVE_FORY_FIVE) {
            ESTIMATED_TIME_OUT.hour = 8;
            ESTIMATED_TIME_OUT.min = 45;
        } else {
            ESTIMATED_TIME_OUT.hour = Math.floor(_timeIN_MIN_SHIFT / 60) + REQ_HOURS;
            ESTIMATED_TIME_OUT.min = _timeIN_MIN_SHIFT - ((ESTIMATED_TIME_OUT.hour - REQ_HOURS) * 60);

            // Calculate undertimes
            var totalUnderTime = {
                hour: 0,
                min: 0
            };

            // Add undertimes to calculate Estimated Time out
            var _timeOUT = [];
            var _timeIN_MIN;
            var _timeOUT_MIN;
            var _minDiff;
            for (i = 0; i < timeOUT.length; i++) {
                if (timeOUT[i] && timeIN[i + 1]) {
                    _timeIN = timeIN[i + 1].split(':');
                    _timeOUT = timeOUT[i].split(':');

                    // Convert to minutes
                    _timeIN_MIN = ((parseInt(_timeIN[0]) * 60) + parseInt(_timeIN[1]));
                    _timeOUT_MIN = ((parseInt(_timeOUT[0]) * 60) + parseInt(_timeOUT[1]));
                    // TODO: CONSIDER NEGATIVE VALUES
                    _minDiff = _timeIN_MIN - _timeOUT_MIN;

                    totalUnderTime.hour = Math.floor(_minDiff / 60);
                    totalUnderTime.min = _minDiff - (totalUnderTime.hour * 60);
                }
            }

            var _estTimeOUT_MIN = (ESTIMATED_TIME_OUT.hour * 60) + ESTIMATED_TIME_OUT.min;
            var _totalUnderTime_MIN = (totalUnderTime.hour) * 60 + totalUnderTime.min;
            var _estimated_time_out = _estTimeOUT_MIN + _totalUnderTime_MIN;

            ESTIMATED_TIME_OUT.hour = Math.floor(_estimated_time_out / 60);
            ESTIMATED_TIME_OUT.min = _estimated_time_out - (ESTIMATED_TIME_OUT.hour * 60);
        }
        

        var _hour = (ESTIMATED_TIME_OUT.hour > 12 ? ESTIMATED_TIME_OUT.hour - 12 : ESTIMATED_TIME_OUT.hour);
        _hour = (_hour < 10 ? ('0' + _hour) : _hour);
        var _zone = (ESTIMATED_TIME_OUT.hour > 12 ? 'PM' : 'AM');
        var _min = ESTIMATED_TIME_OUT.min < 10 ? `0${ESTIMATED_TIME_OUT.min}` : ESTIMATED_TIME_OUT.min;

        return `${_hour}:${_min} ${_zone}`;
    }

    function FetchData() {
        var data = [];
        const max = 200; // Safeguard to prevent infinite loop

        var row;
        var index = 0;
        var record = jQuery.extend(true, {}, EMPTY_RECORD);

        do {
            row = $(`#${TIMETABLE_ROW_ID}${index}`);

            record.DOW = $($(row).find(`span[onclick*="${RECORD_QUERY.DOW}"]`)).text();
            record.DATE = $($(row).find(`span[onclick*="${RECORD_QUERY.DATE}"]`)).text();
            record.SHIFT = $($(row).find(`span[onclick*="${RECORD_QUERY.SHIFT}"]`)).text();

            record.TARDY = $($(row).find(`span[onclick*="${RECORD_QUERY.TARDY}"]`)).text();
            record.UTIME = $($(row).find(`span[onclick*="${RECORD_QUERY.UTIME}"]`)).text();
            record.THOURS = $($(row).find(`span[onclick*="${RECORD_QUERY.THOURS}"]`)).text();
            record.REGWORK = $($(row).find(`span[onclick*="${RECORD_QUERY.REGWORK}"]`)).text();
            record.EHOURS = $($(row).find(`span[onclick*="${RECORD_QUERY.EHOURS}"]`)).text();

            let INS = $($(row).find(`span[onclick*="${RECORD_QUERY.INS}"]`));
            let in_record;
            let in_index = 0;
            do {
                if (in_index > TH_COUNT) {
                    TH_COUNT++;
                }

                in_record = $(INS[in_index]).text().trim();
                record.INS.push(in_record);
                in_index++;
            } while (in_record.length > 0 && in_index < max);

            let OUTS = $($(row).find(`span[onclick*="${RECORD_QUERY.OUTS}"]`));
            let out_record;
            let out_index = 0;
            do {
                out_record = $(OUTS[out_index]).text().trim();
                record.OUTS.push(out_record);
                out_index++;
            } while (out_record.length > 0 && out_index < max);

            record.IS_WEEKEND = record.DOW === 'Sat' ? true : (record.DOW === 'Sun' ? true : false);

            index++;
            if (record.DOW) {
                data.push(record);
                record = jQuery.extend(true, {}, EMPTY_RECORD);
            } else {
                break;
            }
        } while (row.length > 0 && index < max);

        return data;
    }

    function RenderTimeTable() {
        var tr;
        var classes = '';

        for (let i = 1; i < TH_COUNT; i++) {
            $('table thead tr').first().append('<th>Time In/Out</th>');
        }

        DATA.TIMETABLE.forEach((record) => {
            classes = `${record.IS_WEEKEND ? LEGEND.REST_DAY : (record.TARDY !== "-" ? LEGEND.LATE : (record.INS.length > record.OUTS.length ? LEGEND.INCOMPLETE : ""))}`;

            tr = `<tr class="${classes}">`;

            tr += `<td>${record.DOW}</td>`;
            tr += `<td>${record.DATE}</td>`;
            tr += `<td>${record.SHIFT}</td>`;
            tr += `<td>${record.TARDY}</td>`;
            tr += `<td>${record.UTIME}</td>`;
            tr += `<td>${record.THOURS}</td>`;
            tr += `<td>${record.REGWORK}</td>`;
            tr += `<td>${record.EHOURS}</td>`;

            if (record.INS.length === 1 && record.INS[0] === "") {
                for (let i = 0; i < TH_COUNT; i++) {
                    tr += '<td>-</td>';
                }
            } else {
                for (let i = 0; i < record.INS.length; i++) {
                    if (record.INS[i] && record.OUTS[i]) {
                        tr += `<td>${record.INS[i] || '-'} / ${record.OUTS[i] || '-'}</td>`;
                    } else if (record.INS[i] && !record.OUTS[i]) {
                        tr += `<td>${record.INS[i]}</td>`;
                    } else {
                        tr += (i < TH_COUNT ? '<td>-</td>' : '');
                    }
                }
                if (record.INS.length < TH_COUNT) {
                    for (let i = record.INS.length; i < TH_COUNT; i++) {
                        tr += '<td>-</td>';
                    }
                }
            }

            tr += '</tr>';
            $("table tbody").append(tr);
        });

    }

    function dateRangeHandler() {
        // # Input Date format: 2017-01-30
        var DATE_FORMAT_INDEX = {
            MM: 1,
            DD: 2,
            YYYY: 0
        }
        var from = $('#date-range-from').val().split('-');
        var to = $('#date-range-to').val().split('-');

        var oldFrom = JSON.parse($('#RadDateFrom_dateInput_ClientState').val());
        var oldTo = JSON.parse($('#RadDateTo_dateInput_ClientState').val());

        // # Reformat dates
        // Form Date formats: 
        // RadDateFrom_dateInput_ClientState:
        //  "lastSetTextBoxValue":"01/31/2017"
        //  "validationText":"2017-01-31-00-00-00"
        //  "valueAsString":"2017-01-31-00-00-00"
        // RadDateFrom:2017-01-11
        // RadDateFrom_dateInput:01/11/2017
        // RadDateFrom_calendar_SD:[[2017,1,11]]

        oldFrom.lastSetTextBoxValue = `${from[DATE_FORMAT_INDEX.MM]}/${from[DATE_FORMAT_INDEX.DD]}/${from[DATE_FORMAT_INDEX.YYYY]}`;
        oldFrom.validationText = `${from[DATE_FORMAT_INDEX.YYYY]}-${from[DATE_FORMAT_INDEX.MM]}-${from[DATE_FORMAT_INDEX.DD]}-00-00-00`;
        oldFrom.valueAsString = `${from[DATE_FORMAT_INDEX.YYYY]}-${from[DATE_FORMAT_INDEX.MM]}-${from[DATE_FORMAT_INDEX.DD]}-00-00-00`;

        oldTo.lastSetTextBoxValue = `${to[DATE_FORMAT_INDEX.MM]}/${to[DATE_FORMAT_INDEX.DD]}/${to[DATE_FORMAT_INDEX.YYYY]}`;
        oldTo.validationText = `${to[DATE_FORMAT_INDEX.YYYY]}-${to[DATE_FORMAT_INDEX.MM]}-${to[DATE_FORMAT_INDEX.DD]}-00-00-00`;
        oldTo.valueAsString = `${to[DATE_FORMAT_INDEX.YYYY]}-${to[DATE_FORMAT_INDEX.MM]}-${to[DATE_FORMAT_INDEX.DD]}-00-00-00`;

        var RadDateFrom = `${from[DATE_FORMAT_INDEX.YYYY]}-${from[DATE_FORMAT_INDEX.MM]}-${from[DATE_FORMAT_INDEX.DD]}`;
        var RadDateFrom_dateInput = `${from[DATE_FORMAT_INDEX.MM]}/${from[DATE_FORMAT_INDEX.DD]}/${from[DATE_FORMAT_INDEX.YYYY]}`;
        var RadDateFrom_calendar_SD = [[from[DATE_FORMAT_INDEX.YYYY],from[DATE_FORMAT_INDEX.MM],from[DATE_FORMAT_INDEX.DD]]];

        var RadDateTo = `${to[DATE_FORMAT_INDEX.YYYY]}-${to[DATE_FORMAT_INDEX.MM]}-${to[DATE_FORMAT_INDEX.DD]}`;
        var RadDateTo_dateInput = `${to[DATE_FORMAT_INDEX.MM]}/${to[DATE_FORMAT_INDEX.DD]}/${to[DATE_FORMAT_INDEX.YYYY]}`;
        var RadDateTo_calendar_SD = [[to[DATE_FORMAT_INDEX.YYYY],to[DATE_FORMAT_INDEX.MM],to[DATE_FORMAT_INDEX.DD]]];

        // # Update Dates
        $('#RadDateFrom_dateInput_ClientState').val(JSON.stringify(oldFrom));
        $('#RadDateFrom').val(RadDateFrom);
        $('#RadDateFrom_dateInput').val(RadDateFrom_dateInput);
        $('#RadDateFrom_calendar_SD').val(RadDateFrom_calendar_SD);

        $('#RadDateTo_dateInput_ClientState').val(JSON.stringify(oldTo));
        $('#RadDateTo').val(RadDateTo);
        $('#RadDateTo_dateInput').val(RadDateTo_dateInput);
        $('#RadDateTo_calendar_SD').val(RadDateTo_calendar_SD);
        

        $('#ButtonRet').click();
    }

    function renderRange() {
        var DATE_FORMAT_INDEX = {
            MM: 0,
            DD: 1,
            YYYY: 2
        };
        
        let from = DATA.From.lastSetTextBoxValue.split('/');
        let To = DATA.To.lastSetTextBoxValue.split('/');

        // Format: 2017-01-30
        $('#date-range-from').val(`${from[DATE_FORMAT_INDEX.YYYY]}-${from[DATE_FORMAT_INDEX.MM]}-${from[DATE_FORMAT_INDEX.DD]}`);
        $('#date-range-to').val(`${To[DATE_FORMAT_INDEX.YYYY]}-${To[DATE_FORMAT_INDEX.MM]}-${To[DATE_FORMAT_INDEX.DD]}`);
        //$('#date-range-to');
    }
});