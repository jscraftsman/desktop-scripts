= Tested Browsers = 
- Google Chrome
- Microsoft Edge

= Installation Guide (Chrome - One Time Only) =
1. Install CJS (Custom Javascript for Websites) chrome plugin
    > https://chrome.google.com/webstore/detail/custom-javascript-for-web/poakhlngfciodnhlhhgnaaelnpjljija
    > A "cjs" icon should appear beside the address bar (right side of the star)
2. Once the CJS plugin is installed, go to the Electronic time Keeping (Actatek) application and logged in.
    > http://kddpsvrams/timekeeping/WebLogin.aspx
3. Open 'tktac.js' using any text editor and copy all the contents of the file.
4. In the browser, click the 'cjs' icon and replace '// Here You can type your custom JavaScript...' with the contents of 'tktac.js' and click 'save'

= Script Updates =
1. Obtain a copy of the updated 'tktac.js'
2. Open 'tktac.js' using any text editor and copy all the contents of the file.
3. In the browser, click the 'cjs' icon and replace all the text with the contents of the new 'tktac.js' and click 'save'

= Notes =
- If you find any bugs or incorrect calculations please send me a (1)screenshot of the page and (2) description of the issue.