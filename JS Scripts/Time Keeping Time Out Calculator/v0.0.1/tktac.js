/*
 * Name: Time Keeping Time Out Calculator 
 * Description: Calculates the estimated time out for the Actatek (Electronic time Keeping) application if user has multiple time in and time outs
 * Version: 0.0.1 
 */

(function() {
    var VERSION = '0.0.1';

    // Only add the display element if it is not yet added.
    if ($('#estTimeOut').length === 0) {
        $('#LinkButtonPassword').parent().append('<div>Time Keeping Time Out Calculator <small>(v' + VERSION + ')</small></div> ');
        $('#LabelTout').parent().append('<span><strong>Caculated Time Out:</strong> <span id="estTimeOut">N/A</span></span>');
    }

    var i = 0;
    var timeIN = [];
    var timeOUT = [];

    // Get current date for server
    var dateStr = $('#Label1').text().replace('|', '');
    var currentDate = new Date(dateStr);

    var CURRENT_DATE_INDEX = -1;

    // Find current date. Extract index of DOM of current date.
    var dates = $('span[onclick*="daily_date"]');
    var today = currentDate.format('MM/dd/yyyy');
    for (i = 0; i < dates.length; i++) {
        if ($(dates[i]).text() === today) {
            CURRENT_DATE_INDEX = i;
            break;
        }
    }
    if (CURRENT_DATE_INDEX == -1) {
        throw 'Current date not found';
    }

    // Extract time in and time out datas
    var timeStr;
    var inContainers = $(dates[CURRENT_DATE_INDEX]).parent().find('span[onclick*="in"]');
    for (i = 0; i < inContainers.length; i++) {
        timeStr = $(inContainers[i]).text().trim();
        if (timeStr !== '' && timeStr.length < 6) {
            timeIN.push(timeStr);
        }
    }
    if (timeIN.length === 0) {
        throw 'No TIME IN data found';
    }

    var outContainers = $(dates[CURRENT_DATE_INDEX]).parent().find('span[onclick*="out"]');
    for (i = 0; i < outContainers.length; i++) {
        timeStr = $(outContainers[i]).text().trim();
        if (timeStr !== '' && timeStr.length < 6) {
            timeOUT.push(timeStr);
        }
    }

    // HANDLE SHIFT
    var SHIFT = parseInt($(dates[CURRENT_DATE_INDEX]).parent().find('span[onclick*="shift_code"]').text());
    if (isNaN(SHIFT)) {
        throw 'Shift is invalid';
    }

    // Get First Time In.
    // Calculate Estimated Time out (9 hours)
    var _timeIN = timeIN[0].split(':');

    // Handle shift buffer
    var _timeIN_MIN_SHIFT = ((parseInt(_timeIN[0]) * 60) + parseInt(_timeIN[1]));
    _timeIN_MIN_SHIFT = (_timeIN_MIN_SHIFT < ((SHIFT * 60) - 60) ? ((SHIFT * 60) - 60) : _timeIN_MIN_SHIFT);

    var ESTIMATED_TIME_OUT = {
        hour: 0,
        min: 0
    };
    ESTIMATED_TIME_OUT.hour = Math.floor(_timeIN_MIN_SHIFT / 60) + 9;
    ESTIMATED_TIME_OUT.min = _timeIN_MIN_SHIFT - ((ESTIMATED_TIME_OUT.hour - 9) * 60);

    // Calculate undertimes
    var totalUnderTime = {
        hour: 0,
        min: 0
    };

    // Add undertimes to calculate Estimated Time out
    var _timeOUT = [];
    var _timeIN_MIN;
    var _timeOUT_MIN;
    var _minDiff;
    for (i = 0; i < timeOUT.length; i++) {
        if (timeOUT[i] && timeIN[i + 1]) {
            _timeIN = timeIN[i + 1].split(':');
            _timeOUT = timeOUT[i].split(':');

            // Convert to minutes
            _timeIN_MIN = ((parseInt(_timeIN[0]) * 60) + parseInt(_timeIN[1]));
            _timeOUT_MIN = ((parseInt(_timeOUT[0]) * 60) + parseInt(_timeOUT[1]));
            // TODO: CONSIDER NEGATIVE VALUES
            _minDiff = _timeIN_MIN - _timeOUT_MIN;

            totalUnderTime.hour = Math.floor(_minDiff / 60);
            totalUnderTime.min = _minDiff - (totalUnderTime.hour * 60);
        }
    }

    var _estTimeOUT_MIN = (ESTIMATED_TIME_OUT.hour * 60) + ESTIMATED_TIME_OUT.min;
    var _totalUnderTime_MIN = (totalUnderTime.hour) * 60 + totalUnderTime.min;
    var _estimated_time_out = _estTimeOUT_MIN + _totalUnderTime_MIN;

    ESTIMATED_TIME_OUT.hour = Math.floor(_estimated_time_out / 60);
    ESTIMATED_TIME_OUT.min = _estimated_time_out - (ESTIMATED_TIME_OUT.hour * 60);

    var _hour = (ESTIMATED_TIME_OUT.hour > 12 ? ESTIMATED_TIME_OUT.hour - 12 : ESTIMATED_TIME_OUT.hour);
    _hour = (_hour < 10 ? ('0' + _hour) : _hour);
    var _zone = (ESTIMATED_TIME_OUT.hour > 12 ? 'PM' : 'AM');
    var _min = ESTIMATED_TIME_OUT.min;
    $('#estTimeOut').text(_hour + ':' + _min + ' ' + _zone);
})();