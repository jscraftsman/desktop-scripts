@ECHO OFF 
REM Title: checkLM
REM Description: Check version of LM items
REM Author: Gervel Giva
REM Version: 1.0.2

REM Syntax CheckLM <Target Base Path> <DVD Version>

REM Parameters: %1 - Target Base Path  (until contents folder) [KDC / KM / TA]
REM				%2 - Version [1.0 / 2.0 / 3.0 / <EMPTY>]

REM EXAMPLE: CheckLM "E:\P4V\depot\Projects\CDROM\Unverified\Mebius\KM\Contents" 3.0
REM EXAMPLE: CheckLM "E:\P4V\depot\Projects\CDROM\Unverified\Perseus2\KM\Contents" 1.0
REM EXAMPLE: CheckLM "E:\P4V\depot\Projects\CDROM\Unverified\Perseus2\KM\Contents" 1.0
REM EXAMPLE: CheckLM "e:\P4V\depot\Projects\CDROM\Unverified\Perseus2\TA\Contents" 1.0
REM EXAMPLE: CheckLM e:\P4V\depot\Projects\CDROM\Unverified\Perseus2\TA\Contents

ECHO Starting LM version checker

SET VERSION="\%2"
IF "%2"=="" ( 
	ECHO No DVD Version is Entered.
	ECHO.
	SET VERSION=""
)

SET DRIVER_BASE=%1

REM ############# PrnDrv #############
EXPAND %DRIVER_BASE%\PrnDrv\KXDriver\32bit\kxplm32.dl_ %DRIVER_BASE%\PrnDrv\KXDriver\32bit\kxplm32.dll > NUL
ECHO Version of: \PrnDrv\KXDriver\32bit\kxplm32.dll
CALL GetVersion %DRIVER_BASE%\PrnDrv\KXDriver\32bit\kxplm32.dll
ECHO.

EXPAND %DRIVER_BASE%\PrnDrv\KXDriver\64bit\kxplm64.dl_ %DRIVER_BASE%\PrnDrv\KXDriver\64bit\kxplm64.dll > NUL
ECHO Version of: \PrnDrv\KXDriver\64bit\kxplm64.dll
CALL GetVersion %DRIVER_BASE%\PrnDrv\KXDriver\64bit\kxplm64.dll
ECHO.

EXPAND "%DRIVER_BASE%\PrnDrv\K-XPS Driver\32bit\kxplm32.dl_" "%DRIVER_BASE%\PrnDrv\K-XPS Driver\32bit\kxplm32.dll" > NUL
ECHO Version of: \PrnDrv\K-XPS Driver\32bit\kxplm32.dll
CALL GetVersion "%DRIVER_BASE%\PrnDrv\K-XPS Driver\32bit\kxplm32.dll"
ECHO.

EXPAND "%DRIVER_BASE%\PrnDrv\K-XPS Driver\64bit\kxplm64.dl_" "%DRIVER_BASE%\PrnDrv\K-XPS Driver\64bit\kxplm64.dll" > NUL
ECHO Version of: \PrnDrv\K-XPS Driver\64bit\kxplm64.dll
CALL GetVersion "%DRIVER_BASE%\PrnDrv\K-XPS Driver\64bit\kxplm64.dll"
ECHO.

REM ############# PrnDrv_J #############

IF EXIST %DRIVER_BASE%\PrnDrv_J\KXDriver\32bit\kxplm32.dl_ (
	EXPAND %DRIVER_BASE%\PrnDrv_J\KXDriver\32bit\kxplm32.dl_ %DRIVER_BASE%\PrnDrv_J\KXDriver\32bit\kxplm32.dll > NUL
	ECHO Version of: \PrnDrv_J\KXDriver\32bit\kxplm32.dll
	CALL GetVersion %DRIVER_BASE%\PrnDrv_J\KXDriver\32bit\kxplm32.dll
	ECHO.
)

IF EXIST %DRIVER_BASE%\PrnDrv_J\KXDriver\64bit\kxplm64.dl_ (
	EXPAND %DRIVER_BASE%\PrnDrv_J\KXDriver\64bit\kxplm64.dl_ %DRIVER_BASE%\PrnDrv_J\KXDriver\64bit\kxplm64.dll > NUL
	ECHO Version of: \PrnDrv_J\KXDriver\64bit\kxplm64.dll
	CALL GetVersion %DRIVER_BASE%\PrnDrv_J\KXDriver\64bit\kxplm64.dll
	ECHO.
)

IF EXIST "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\32bit\kxplm32.dl_" (
	EXPAND "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\32bit\kxplm32.dl_" "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\32bit\kxplm32.dll" > NUL
	ECHO Version of: \PrnDrv_J\K-XPS Driver\32bit\kxplm32.dll
	CALL GetVersion "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\32bit\kxplm32.dll"
	ECHO.
)

IF EXIST "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\64bit\kxplm64.dl_" (
	EXPAND "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\64bit\kxplm64.dl_" "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\64bit\kxplm64.dll" > NUL
	ECHO Version of: \PrnDrv_J\K-XPS Driver\64bit\kxplm64.dll
	CALL GetVersion "%DRIVER_BASE%\PrnDrv_J\K-XPS Driver\64bit\kxplm64.dll"
	ECHO.
)

REM ############# Setup Folder #############


ECHO Version of: \Setup\KXPLM32.dll
set "setupLM32=%DRIVER_BASE%\Setup\KXPLM32.dll"
set "setupLM32=%setupLM32:\=\\%"
WMIC datafile where Name="%setupLM32%" get Version
ECHO.

ECHO Version of: \Setup\KXPLM64.dll
set "setupLM64=%DRIVER_BASE%\Setup\KXPLM64.dll"
set "setupLM64=%setupLM64:\=\\%"
WMIC datafile where Name="%setupLM64%" get Version
ECHO.

ECHO Task complete.