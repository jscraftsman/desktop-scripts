@ECHO OFF
REM Sript Name: findFromFiles.bat
REM Description: 
REM	Finds a specific string from multiple files (.js and .vue)
REM	Must be run in the root directory of the project
REM Author: Gervel Giva

REM Parameters:
REM	%1 - String / Keyword to be find. Must be enclosed with double qoutes

grep -Rn %1 **/*.js **/*.vue 