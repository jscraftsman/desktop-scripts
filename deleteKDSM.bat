@ECHO OFF 
REM Title: deleteKDSM
REM Description: Description template
REM Author: givag

ECHO STOPPING KDSERVICE...
SC STOP KDSERVICE > NUL

ECHO REMOVING KDSERVICE...
DEL "C:\Program Files\KDService\bin\KDService.exe"

IF EXIST C:\ProgramData\KDService\KDService.log (
	ECHO REMOVING KDSERVICE LOG...
	DEL C:\ProgramData\KDService\KDService.log
)

ECHO REMOVING STATUS MONITOR COMPONENTS...
IF EXIST C:\Windows\System32\spool\drivers\x64\3\KDSSTM.exe (
	DEL C:\Windows\System32\spool\drivers\x64\3\KDSSTM.exe
	DEL C:\Windows\System32\spool\drivers\x64\3\KDSSTM.ini
	DEL C:\Windows\System32\spool\drivers\x64\3\KDSSTMS.ini
) ELSE (
	IF EXIST C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.exe (
		DEL C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.exe
		DEL C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.ini
		DEL C:\Windows\System32\spool\drivers\W32X86\3\KDSSTMS.ini
	)
)
