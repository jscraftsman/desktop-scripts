@ECHO OFF 
REM Title: KLSM - KDService, Language Monitor, and Status Monitor Manager
REM Description: A batch script to manage KLS Options
REM Author: nameless

REM Changelog
REM 1.0.0 - Initial Version
REM 1.0.1 - Added [12] Open "Devices and Printers" and [9] Restart Spooler 

TITLE KLS Manager

SET KLSM_VER=1.0.2

:MENU
ECHO.
ECHO ========================================
ECHO KLS Manager (%KLSM_VER%) - Options
ECHO ========================================
ECHO [1] Exit
ECHO [2] Clear Console (or type 'cls')
ECHO [3] Version KLS 
ECHO [4] Remove KLS Components 
ECHO [5] Remove KDService Components
ECHO [6] Remove Language Monitor Components
ECHO [7] Remove Status Monitor Components  
ECHO [8] Clear KDService Logs
ECHO [9] Restart Spooler
ECHO [10] Get IP (IPv4)
ECHo [11] Shutdown
ECHo [12] Open "Devices and Printers"

ECHO.
SET /P input="[Input] "
ECHO.

GOTO HANDLE_INPUT


REM Short Menu
:SHORT_MENU
ECHO.
ECHO ========================================
ECHO KLS Manager (%KLSM_VER%)
ECHO ========================================
ECHO [0] Show Detailed Menu
SET /P input="[Input] "
ECHO.

GOTO HANDLE_INPUT
REM END "Short Menu"

:HANDLE_INPUT
REM [0]Show Menue
IF %input% EQU 0 (
    GOTO MENU
)

REM [1] Exit
IF %input% EQU 1 (
    GOTO END
)

REM [2] Clear Console
IF "%input%"=="cls" (
    GOTO :CLEAR_CONSOLE
)

IF %input% EQU 2 (
    GOTO :CLEAR_CONSOLE
)

REM [3] Version KLS
IF %input% EQU 3 (
    GOTO VERSION_KLS
)

REM [4] Remove KLS Components 
IF %input% EQU 4 (
    ECHO [INFO] Not Implemented
)

REM [5] Remove KDService Components
IF %input% EQU 5 (
    GOTO REMOVE_KDS
)

REM [6] Remove Language Monitor Components
IF %input% EQU 6 (
    GOTO REMOVE_LM
)

REM [7] Remove Status Monitor Components  
IF %input% EQU 7 (
    GOTO REMOVE_SM
)

REM [8] Clear KDService Logs
IF %input% EQU 8 (
    GOTO REMOVE_KDS_LOGS
)

REM [9] Restart Spool
IF %input% EQU 9 (
    GOTO RES_SPOOL
) 

REM [10] Get IP (IPv4)
IF %input% EQU 10 (
    GOTO GET_IP
) 

REM [11] Shutdown
IF %input% EQU 11 (
    GOTO PREPARE_SHUTDOWN
) 

REM [12] Open "Devices and Printers"
IF %input% EQU 12 (
    GOTO OPEN_DAP
) ELSE (
    ECHO.
    ECHO -------------------------
    ECHO [%input%] Invalid input.
    ECHO -------------------------

    ECHO.
    GOTO SHORT_MENU
)


REM ========================================================

REM [12] Open "Devices and Printers"
:OPEN_DAP
ECHO [Start] Opening "Devices and Printers"
explorer shell:::{A8A91A66-3A7D-4424-8D24-04E180695C7A}
ECHO [Completed]
GOTO SHORT_MENU
REM END [12]

REM [11] Shutdown
:PREPARE_SHUTDOWN
ECHO [Start] Start shutdown sequence...
SET /P skey="[Enter shutdown key] "
IF "%skey%"=="shutdown" (
    SHUTDOWN -s -t 30
    ECHO [Success] Shutting down...
    ECHO System will shutdown in 30 seconds!
    GOTO END
)ELSE (
    ECHO Incorrect shutdown key!
    ECHO Returning to menu... 
    GOTO SHORT_MENU
)
REM END [11]

REM [10] Get IP (IPv4)
:GET_IP
ECHO [Start] Get IP
IPCONFIG | FIND "IPv4"
ECHO [Completed]
GOTO SHORT_MENU
REM END [10]

REM [9] Restart Spool
:RES_SPOOL
ECHO [Start] Restarting spooler
SC STOP SPOOLER
ECHO Spooler stopped
TIMEOUT 3 > NUL
ECHO Starting spooler
SC START SPOOLER
ECHO [Completed]
GOTO SHORT_MENU
REM END [9]

REM [8] Clear KDService Logs
:REMOVE_KDS_LOGS
ECHO [Start] Clearing KDService logs...

:CHECK_KDS
TASKLIST | FIND "KDService.exe" > NUL
IF ERRORLEVEL 1 (
  GOTO KDS_STOPPED
) ELSE (
  ECHO KDService is still running.. stopping KDService...
  SC STOP kdservice
  TIMEOUT 3 > NUL
  GOTO CHECK_KDS
)

:KDS_STOPPED
ECHO Removing KDService log file
DEL "C:\ProgramData\KDService\KDService.log"
IF ERRORLEVEL 1 (
  ECHO Failed to remove log file...
  IF EXIST "C:\ProgramData\KDService\KDService.log" (
      GOTO CHECK_KDS
  ) ELSE (
      GOTO LOG_REMOVED
  )
  
) ELSE (
  ECHO Log file removed...
  GOTO LOG_REMOVED
)

:LOG_REMOVED
ECHO Restarting KDService...
SC START kdservice

ECHO [Completed]
GOTO SHORT_MENU
REM END [8]

REM [7] Remove Status Monitor Components
:REMOVE_SM
ECHO [Start] Removing Status Monitor Components

IF EXIST "C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" (
    IF EXIST "C:\Windows\System32\spool\drivers\x64\3\KDSSTM.exe" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\x64\3\KDSSTM.exe"
    ) ELSE (
        ECHO [INFO] KDSSTM.exe does not exist. No need to remove.
    )
    IF EXIST "C:\Windows\System32\spool\drivers\x64\3\KDSSTM.ini" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\x64\3\KDSSTM.ini"
    ) ELSE (
        ECHO [INFO] KDSSTM.ini does not exist. No need to remove.
    )
    IF EXIST "C:\Windows\System32\spool\drivers\x64\3\KDSSTMS.ini" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\x64\3\KDSSTMS.ini"
    ) ELSE (
        ECHO [INFO] KDSSTMS.ini does not exist. No need to remove.
    )
) ELSE (
    IF EXIST "C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.exe" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.exe"
    ) ELSE (
        ECHO [INFO] KDSSTM.exe does not exist. No need to remove.
    )
    IF EXIST "C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.ini" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.ini"
    ) ELSE (
        ECHO [INFO] KDSSTM.ini does not exist. No need to remove.
    )
    IF EXIST "C:\Windows\System32\spool\drivers\W32X86\3\KDSSTMS.ini" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\W32X86\3\KDSSTMS.ini"
    ) ELSE (
        ECHO [INFO] KDSSTMS.ini does not exist. No need to remove.
    )
)

ECHO [Completed]
GOTO SHORT_MENU
REM END [7]

REM [6] Remove Language Monitor Components
:REMOVE_LM
ECHO [Start] Removing Language Monitor Components

SC STOP SPOOLER
TIMEOUT 3 > NUL

ECHO %PROCESSOR_ARCHITECTURE% | FIND /i "x86" > nul
IF %ERRORLEVEL%==0 (
	ECHO Removing Language Monitor [32-bit \Windows\System32]
    IF EXIST "C:\Windows\System32\spool\drivers\W32X86\3\KXPLM32.dll" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\W32X86\3\KXPLM32.dll"
    ) ELSE (
        ECHO [INFO] \spool\drivers\W32X86\3\KXPLM32.dll does not exist. No need to remove.
    )
	IF EXIST "C:\Windows\System32\KXPLM32.dll" (
        DEL /Q /F "C:\Windows\System32\KXPLM32.dll"
    ) ELSE (
        ECHO [INFO] \System32\KXPLM32.dll does not exist. No need to remove.
    )
) ELSE (
	ECHO Removing Language Monitor [64-bit \Windows\System32]
    IF EXIST "C:\Windows\System32\spool\drivers\x64\3\KXPLM64.dll" (
        DEL /Q /F "C:\Windows\System32\spool\drivers\x64\3\KXPLM64.dll"
    ) ELSE (
        ECHO [INFO] \spool\drivers\x64\3\KXPLM64.dll does not exist. No need to remove.
    )
    IF EXIST "C:\Windows\System32\KXPLM64.dll" (
        DEL /Q /F "C:\Windows\System32\KXPLM64.dll"
    ) ELSE (
        ECHO [INFO] \System32\KXPLM64.dll does not exist. No need to remove.
    )
)

SC START SPOOLER

ECHO [Completed]
GOTO SHORT_MENU
REM END [6]

REM [5] Remove KDService Components
:REMOVE_KDS
ECHO [Start] Removing KDService Components
IF EXIST "C:\\Program Files\\KDService\\bin\\KDService.exe" (
    SC STOP KDSERVICE
    SC DELETE KDSERVICE
    TIMEOUT 3 > NUL
    RMDIR /S /Q "C:\\Program Files\\KDService\\bin"
    RMDIR /S /Q "C:\\Program Files\\KDService"
    ECHO [INFO] KDService Components Removed.
) ELSE (
    ECHO [INFO] KDService does not exist. No need to remove.
)

ECHO [Comleted]
GOTO SHORT_MENU
REM END [5]

REM [3] Version KLS
:VERSION_KLS
ECHO [Start] Getting Version of KLS components
ECHO.
ECHO KDService
WMIC datafile where Name="C:\\Program Files\\KDService\\bin\\KDService.exe" get Version
REM KDService Path: [C:\Program Files\KDService\bin\KDService.exe]

ECHO Status Monitor
IF EXIST "C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" (
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe" get Version
	REM Status Monitor Path: [C:\Windows\System32\spool\drivers\x64\3\KDSSTM.exe]
) ELSE (
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KDSSTM.exe" get Version
	REM Status Monitor Path: [C:\Windows\System32\spool\drivers\W32X86\3\KDSSTM.exe]
)

ECHO %PROCESSOR_ARCHITECTURE% | FIND /i "x86" > nul
IF %ERRORLEVEL%==0 (
	ECHO Language Monitor [32-bit \Windows\System32]
	WMIC datafile where Name="C:\\Windows\\System32\\KXPLM32.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\KXPLM32.dll]
	
	ECHO Language Monitor [32-bit \Windows\System32\spool\drivers\W32X86\3]
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KXPLM32.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\spool\drivers\W32X86\3\KXPLM32.dll]
) ELSE (
	ECHO Language Monitor [64-bit \Windows\System32]
	WMIC datafile where Name="C:\\Windows\\System32\\KXPLM64.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\KXPLM64.dll]
	
	ECHO Language Monitor [64-bit \Windows\System32\spool\drivers\x64\3]
	WMIC datafile where Name="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KXPLM64.dll" get Version
	REM Language Monitor Path: [C:\Windows\System32\spool\drivers\x64\3\KXPLM64.dll]
)

TASKLIST | FIND /i "KDS"
IF %ERRORLEVEL%==0 (
	ECHO [Windows Service] KDService is running
) ELSE (
	ECHO [Windows Service] KDService is NOT running!
)

ECHO.
ECHO [Completed]
GOTO SHORT_MENU
REM END [3]

REM [2] Clear Console
:CLEAR_CONSOLE
CLS
GOTO SHORT_MENU
REM END [2]

REM [1] Exit
:END
ECHO Bye!
REM END [1]