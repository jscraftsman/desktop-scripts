@ECHO OFF
REM Title: updateKDS
REM Description: A batch file that will update the KDService 1.1 file [Release Version].
REM Author: Gervel Giva
REM Version: 1.0


REM PATHS
SET VERSION=1.1

SET LOCAL_KDS_1_0_COPY="E:\P4 Workstations\depot\Projects\KMC\KDService\KDService\bin\Release\KDService.exe"
SET LOCAL_KDS_1_1_COPY="E:\P4 Workstations\depot\Projects\KMC\KDService_1.1\KDService\bin\Release\KDService.exe"

SET KDS_LOG="C:\ProgramData\KDService\KDService.log"

IF "%~1"=="" (
	GOTO USAGE
	PAUSE
) ELSE (
	GOTO HANDLE_VERSION
)

:HANDLE_VERSION
IF %1% EQU 1.0 (
	SET VERSION=1.0
	SET LOCAL_KDS_COPY=%LOCAL_KDS_1_1_COPY%
	GOTO END
	GOTO HANDLE_UPDATE
) ELSE (
	IF %1% EQU 1.1 (
		SET VERSION=1.1
		SET LOCAL_KDS_COPY=%LOCAL_KDS_1_1_COPY%
		GOTO HANDLE_UPDATE
	) ELSE (
		ECHO Invalid version...
		GOTO USAGE
	)
)


:HANDLE_UPDATE
ECHO.
SET COMMON_PATH="\\10.191.21.45\Installers\KDSERVICE - CLIENT SERVER BUILDS\KDService\%VERSION%\Release\KDService.exe"

IF EXIST "%LOCAL_KDS_COPY%" (
	ECHO A local build of KDService is present at: %LOCAL_KDS_COPY%
	ECHO.
	SET /P USE_LOCAL=Use local build of KDService(Y/N)?
	
	IF /I "%USE_LOCAL%" EQU "Y" (
		CLS
		ECHO ^> Replacing KDService 1.1 common copy.
		XCOPY /Y %LOCAL_KDS_1_1_COPY% %COMMON_PATH% > NUL
		REM CHECK IF SUCCESS
	)
)

GOTO CHECK_KDS

:CHECK_KDS
TASKLIST | FIND "KDService.exe" > NUL
IF %ERRORLEVEL% EQU 1 (
  ECHO ^> KDService is no longer running...
  GOTO KDS_STOPPED
) ELSE (
  ECHO ^> KDService is still running.. Stopping KDService...
  SC STOP kdservice > NUL
  TIMEOUT 3 > NUL
  GOTO CHECK_KDS
)

:KDS_STOPPED
ECHO ^> Updating KDService binary file...
XCOPY /Y \\10.191.21.45\"Installers\KDSERVICE - CLIENT SERVER BUILDS\KDService\1.1\Release\KDService.exe" "C:\Program Files\KDService\bin\KDService.exe" > NUL
IF %ERRORLEVEL% EQU 1 (
  ECHO ^> Failed to update KDService...
  GOTO CHECK_KDS
) ELSE (
  echo ^> KDService updated... Clearing KDS Log
  GOTO REMOVE_LOGFILE  
)

:REMOVE_LOGFILE
DEL %KDS_LOG% > NUL
IF %ERRORLEVEL% EQU 0 (
	GOTO LOG_REMOVED
) ELSE (
	TIMEOUT 3 > NUL
	GOTO REMOVE_LOGFILE
)


:LOG_REMOVED
ECHO ^> Starting KDService...
SC START kdservice > NUL
GOTO END
  
:USAGE
ECHO.
ECHO DESCRIPTION:
ECHO 	updateKDS is a script that update the installed KDService binary file either by version 1.0 or version 1.1
ECHO.
ECHO USAGE:
ECHO 	updateKDS [version=1.0 ^| 1.1]
ECHO.
ECHO 	The option [version] is for the version of KDService that will be used. It can be 1.0 or 1.1
ECHO.
ECHO EXAMPLE:
ECHO 	updateKDS 1.1
GOTO END

:END
