@ECHO OFF
::REM Title: openKDS
::REM Description: A batch file that will simply open the KDService log file
::REM Author: Gervel Giva

IF EXIST "C:\Program Files (x86)\Notepad++\notepad++.exe" (
	"C:\Program Files (x86)\Notepad++\notepad++.exe" "C:\ProgramData\KDService\KDService.log"
) ELSE (
	START NOTEPAD C:\ProgramData\KDService\KDService.log
)
