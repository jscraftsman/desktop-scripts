@ECHO OFF 
REM Title: LMtoDVD
REM Description: Add LM Build to DVD
REM Author: Gervel Giva
REM Version: 1.0

REM Syntax LMtoDVD <Target Base Path> <DVD Version>

REM Parameters: %1 - Target Base Path  (until contents folder) [KDC / KM / TA]
REM				%2 - Version [1.0 / 2.0 / 3.0 / <EMPTY>]

REM EXAMPLE: LMtoDVD "E:\P4V\depot\Projects\CDROM\Unverified\Mebius\KM\Contents" 3.0

SET LM_VERSION=v1.0.3213.0
ECHO Using LM Version %LM_VERSION%
SET VERSION="\%2"
IF "%2"=="" ( 
	echo No DVD Version is Entered.
	SET VERSION=""
)
SET LM_BASE_BUILD="E:\Builds\LM\[Common]\%LM_VERSION%"
ECHO Base LM Build at: %LM_BASE_BUILD%

SET DRIVER_BASE=%1

SET LM_BUILD_32="%LM_BASE_BUILD%\i386\kxplm32.dl_"
SET LM_BUILD_64="%LM_BASE_BUILD%\x64\kxplm64.dl_"

ECHO.
ECHO          Transfer starting...
ECHO.
SET UPDATE_STR=updated
REM KXDriver
XCOPY /Y %LM_BUILD_32% %DRIVER_BASE%"\PrnDrv%VERSION%\KXDriver\32bit\XP and newer\kxplm32.dl_" > NUL
IF ERRORLEVEL 0 (	
	SET UPDATE_STR=updated
) ELSE (
	SET UPDATE_STR=update failed
)
ECHO [     1] [32bit: PrnDrv\KXDriver] %UPDATE_STR%.

XCOPY /Y %LM_BUILD_64% %DRIVER_BASE%"\PrnDrv%VERSION%\KXDriver\64bit\XP and newer\kxplm64.dl_" > NUL
IF ERRORLEVEL 0 (	
	SET UPDATE_STR=updated
) ELSE (
	SET UPDATE_STR=update failed
)
ECHO [     2] [64bit: PrnDrv\KXDriver] %UPDATE_STR%.

REM K-XPS Driver
XCOPY /Y %LM_BUILD_32% %DRIVER_BASE%"\PrnDrv%VERSION%\K-XPS Driver\32bit\Vista and newer\kxplm32.dl_" > NUL
IF ERRORLEVEL 0 (	
	SET UPDATE_STR=updated
) ELSE (
	SET UPDATE_STR=update failed
)
ECHO [     3] [32bit: PrnDrv\K-XPS Driver] %UPDATE_STR%.

XCOPY /Y %LM_BUILD_64% %DRIVER_BASE%"\PrnDrv%VERSION%\K-XPS Driver\64bit\Vista and newer\kxplm64.dl_" > NUL
IF ERRORLEVEL 0 (	
	SET UPDATE_STR=updated
) ELSE (
	SET UPDATE_STR=update failed
)
ECHO [     4] [64bit: PrnDrv\K-XPS Driver] %UPDATE_STR%.

REM ######################################
REM PrnDrv_J

IF EXIST %DRIVER_BASE%\PrnDrv_J (
	REM KXDriver
	XCOPY /Y %LM_BUILD_32% %DRIVER_BASE%"\PrnDrv_J%VERSION%\KXDriver\32bit\XP and newer\kxplm32.dl_" > NUL
	IF ERRORLEVEL 0 (	
		ECHO [     5] [32bit: PrnDrv_J\KXDriver] updated.
	)
	XCOPY /Y %LM_BUILD_64% %DRIVER_BASE%"\PrnDrv_J%VERSION%\KXDriver\64bit\XP and newer\kxplm64.dl_" > NUL
	IF ERRORLEVEL 0 (	
		ECHO [     6] [64bit: PrnDrv_J\KXDriver] updated.
	)
	REM K-XPS Driver
	XCOPY /Y %LM_BUILD_32% %DRIVER_BASE%"\PrnDrv_J%VERSION%\K-XPS Driver\32bit\Vista and newer\kxplm32.dl_" > NUL
	IF ERRORLEVEL 0 (	
		ECHO [     7] [32bit: PrnDrv_J\K-XPS Driver] updated.
	)
	XCOPY /Y %LM_BUILD_64% %DRIVER_BASE%"\PrnDrv_J%VERSION%\K-XPS Driver\64bit\Vista and newer\kxplm64.dl_" > NUL
	IF ERRORLEVEL 0 (	
		ECHO [     8] [64bit: PrnDrv_J\K-XPS Driver] updated.
	)
)

REM ##############################
REM Setup Folder
IF EXIST %DRIVER_BASE%\Setup (
	ECHO.
	ECHO          Updating Setup folder
	XCOPY /Y %LM_BASE_BUILD%\KXPLM32.dll %DRIVER_BASE%\Setup\KXPLM32.dll > NUL
	IF ERRORLEVEL 0 (	
		SET UPDATE_STR=updated
	) ELSE (
		SET UPDATE_STR=update failed
	)
	ECHO [     9] [Setup\KXPLM32.dll %UPDATE_STR%.
	XCOPY /Y %LM_BASE_BUILD%\KXPLM64.dll %DRIVER_BASE%\Setup\KXPLM64.dll > NUL
	IF ERRORLEVEL 0 (	
		SET UPDATE_STR=updated
	) ELSE (
		SET UPDATE_STR=update failed
	)
	ECHO [    10] [Setup\KXPLM64.dll %UPDATE_STR%.
)


ECHO.
ECHO          Transfer complete...