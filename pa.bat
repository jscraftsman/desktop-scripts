@ECHO OFF 
REM Title: PA (Project Alpha)
REM Description: A batch script that setup development environment for Project Alpha
REM Author: nameless

ECHO TABS
ECHO [1] E2E (and set path)
ECHO [2] GIT
ECHO [3] Unit
ECHO [4] Web Server
ECHO [5] Selenium

ECHO.
SET /P input="Select Tab: "

IF %input% EQU 1 (
    CD /D "E:\Nodejs Projects\project-alpha"
    ConEmuC -GuiMacro Rename 0 "E2E"
    CLS
)
IF %input% EQU 2 (
    ConEmuC -GuiMacro Rename 0 "GIT"
    CLS
) 
IF %input% EQU 3 (
    ConEmuC -GuiMacro Rename 0 "Unit"
    CLS
) 
IF %input% EQU 4 (
    ConEmuC -GuiMacro Rename 0 "Web Server"
    CLS
    sails lift
) 
IF %input% EQU 5 (
    ConEmuC -GuiMacro Rename 0 "Selenium"
    CLS
    npm run sel
)