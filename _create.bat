@ECHO OFF
::REM Title: create
::REM Description: A script that will create a new batch script
::REM Author: Gervel Giva

IF [%1] EQU [] (
	GOTO USAGE
) ELSE (
	GOTO SETUP
)


:USAGE
ECHO Guide

:SETUP
SET FILE=%1.bat

	

IF [%2] EQU [] (
	SET FILE_PATH=C:\Scripts
	GOTO CREATE
) ELSE (
	SET FILE_PATH=%2
	IF EXIST "%FILE_PATH%" (
		IF EXIST "%FILE_PATH%\%FILE%" (
			ECHO File already exist.
			GOTO END
		) ELSE (
			GOTO CREATE
		)
		
	) ELSE (
		ECHO.
		ECHO The supplied folder does not exist!
		ECHO %FILE_PATH%
	)
	
)

:CREATE
(
	ECHO @ECHO OFF 
	ECHO REM Title: %1
	ECHO REM Description: Description template
	IF %USERNAME%=="givag" (
		ECHO REM Author: Gervel Giva
	) ELSE (
		ECHO REM Author: %USERNAME%
	)
) >> "%FILE_PATH%\%FILE%"

IF EXIST "C:\Program Files (x86)\Notepad++\notepad++.exe" (
	"C:\Program Files (x86)\Notepad++\notepad++.exe" "%FILE_PATH%\%FILE%"
) ELSE (
	START NOTEPAD C:\ProgramData\KDService\KDService.log
)


:END
ECHO.
ECHO Script complete


