@ECHO OFF 
REM Title: checkIssue1
REM Description: A batch script that will check some possible cause for issue #1 <link>
REM Author: Gervel Giva
REM Version: 1.0
REM Link: http://kddpsvrspprod/sites/KDDP/SD2Div/SD3/SD33/KDService Wiki/Issue-1---In-Client-Server-environment,-Status-monitor-will-not-show-on-the-Client-when-a-printing.aspx
REM Issue #1

REM Issue:
REM In Client-Server environment, SM will not show on the Client when a printing.

REM Possible Cause:
REM 1. Status Monitor is not installed on the client.
REM 2. KDS is not running on either the server or the client.
REM 3. LM version of either the server or client is not version 1.1 or higher
REM 4. SM version of either the server or client is not version 5.0 or higher
REM 5. KDS version of either the server or client is not version 1.1 or higher
REM 6. KDS has encountered an OpenService Error, the error is triggered if KDS does not have proper permission to run/kick the SM on an Client-Server environment.

ECHO.
ECHO DESCRIPTION:
ECHO 	This script is a check to a possible cause of the issue: "In Client-Server environment, SM will not show on the Client when a printing."

:INIT
CLS
ECHO Issue #1 Checker is running...
SET KDS_PATH="C:\\Program Files\\KDService\\bin\\KDService.exe"
ECHO %PROCESSOR_ARCHITECTURE% | FIND /i "x86"
IF %ErrorLevel% EQU 0 (
 SET ARCH=x86
 SET SM_PATH="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KDSSTM.exe"
 SET LM_PATH="C:\\Windows\\System32\\KXPLM32.dll"
 IF NOT EXIST %LM_PATH% (
  SET LM_PATH="C:\\Windows\\System32\\spool\\drivers\\W32X86\\3\\KXPLM32.dll"
 )
) ELSE (
 SET ARCH=x64
 SET SM_PATH="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KDSSTM.exe"
 SET LM_PATH="C:\\Windows\\System32\\KXPLM64.dll"
 IF NOT EXIST %LM_PATH% (
  SET LM_PATH="C:\\Windows\\System32\\spool\\drivers\\x64\\3\\KXPLM32.dll"
 )
)
GOTO CHECK_1

:CHECK_1:
ECHO.
ECHO [1] Checking if Status Monitor is installed...
IF EXIST %SM_PATH% (
 ECHO [OK] Status Monitor is installed.
 GOTO CHECK_2
) ELSE (
 ECHO [FAILED] Status Monitor not is installed.
 GOTO END
)

:CHECK_2:
ECHO [2] Checking if KDService is currently running...
TASKLIST | FIND "KDService.exe" > NUL
IF %ErrorLevel% EQU 0 (
 ECHO [OK] KDService is running.
 GOTO CHECK_3
) ELSE (
 ECHO [FAILED] KDService is either not running or not installed.
 GOTO END
)


:CHECK_3
ECHO [3] Checking if Language Monitor is version 1.1 or higher...
WMIC datafile where Name=%LM_PATH% get Version | FIND /i "1.1" > NUL
IF %ErrorLevel% EQU 0 (
 ECHO [OK] Language Monitor is version 1.1 or higher.
 GOTO CHECK_4
) ELSE (
 ECHO [FAILED] Language Monitor is either not installed or its version is not 1.1 up.
 GOTO END
)

:CHECK_4
ECHO [4] Checking if Status Monitor is version 5.0 or higher...
WMIC datafile where Name=%SM_PATH% get Version | FIND /i "5.0" > NUL
IF %ErrorLevel% EQU 0 (
 ECHO [OK] Status Monitor is version 5.0 or higher.
 GOTO CHECK_5
) ELSE (
 ECHO [FAILED] Status Monitor is either not installed or its version is not 5.0 up.
 GOTO END
)

:CHECK_5
ECHO [5] Checking if KDService is version 1.1 or higher...
WMIC datafile where Name=%KDS_PATH% get Version | FIND /i "1.1" > NUL
IF %ErrorLevel% EQU 0 (
 ECHO [OK] KDService is version 1.1 or higher.
 GOTO CHECK_6
) ELSE (
 ECHO [FAILED] KDService is either not installed or its version is not 1.1 up.
 GOTO END
)


:CHECK_6
ECHO [6] Checking if KDService has proper permission...
SC SDSHOW KDSERVICE | FIND /i "D:(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWLOCRRC;;;SU)S:D:(A;;CCLC;;;AU)(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWLOCRRC;;;SU)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"
IF %ErrorLevel% EQU 0 (
 ECHO [OK] KDService has proper permission.
) ELSE (
 ECHO [FAILED] KDService has an improper permission. 
 ECHO Confirm this by running Debug view(as administrator) on the server side and look for an "OpenServer Error = 5" in the logs
)
GOTO END

:END
ECHO.
ECHO ^> Script complete...
PAUSE