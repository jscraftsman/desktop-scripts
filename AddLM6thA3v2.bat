@ECHO OFF 
REM Title: AddLM6thA3v2
REM Description: Add LM to 6thA3 v2.0
REM Author: Gervel Giva

SET LM_BUILD_32="E:\Builds\LM\v1.0.1213.4\i386\kxplm32.dl_"
SET LM_BUILD_64="E:\Builds\LM\v1.0.1213.4\x64\kxplm64.dl_"

ECHO Adding LM to PrnDrv 2.0...
REM KXDriver
XCOPY /Y  %LM_BUILD_32% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv\2.0\KXDriver\32bit\XP and newer\kxplm32.dl_"
ECHO [     1] 32bit LM for PrnDrv\2.0\KXDriver added.
XCOPY /Y  %LM_BUILD_64% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv\2.0\KXDriver\64bit\XP and newer\kxplm64.dl_"
ECHO [     2] 64bit LM for PrnDrv\2.0\KXDriver added.

REM K-XPS Driver
XCOPY /Y  %LM_BUILD_32% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv\2.0\K-XPS Driver\32bit\Vista and newer\kxplm32.dl_"
ECHO [     3] 32bit LM for PrnDrv\2.0\K-XPS Driver added.
XCOPY /Y  %LM_BUILD_64% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv\2.0\K-XPS Driver\64bit\Vista and newer\kxplm64.dl_"
ECHO [     4] 64bit LM for PrnDrv\2.0\K-XPS Driver added.

ECHO Adding LM to PrnDrv_J 2.0...
REM KXDriver
XCOPY /Y  %LM_BUILD_32% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv_J\2.0\KXDriver\32bit\XP and newer\kxplm32.dl_"
ECHO [     5] 32bit LM for PrnDrv_J\2.0\KXDriver added.
XCOPY /Y  %LM_BUILD_64% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv_J\2.0\KXDriver\64bit\XP and newer\kxplm64.dl_"
ECHO [     6] 64bit LM for PrnDrv_J\2.0\KXDriver added.


REM K-XPS Driver
XCOPY /Y  %LM_BUILD_32% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv_J\2.0\K-XPS Driver\32bit\Vista and newer\kxplm32.dl_"
ECHO [     7] 32bit LM for PrnDrv_J\2.0\K-XPS Driver added.
XCOPY /Y  %LM_BUILD_64% "E:\P4V\depot\Projects\CDROM\Unverified\6thPrinterA3\KM\Contents\PrnDrv_J\2.0\K-XPS Driver\64bit\Vista and newer\kxplm64.dl_"
ECHO [     8] 64bit LM for PrnDrv_J\2.0\K-XPS Driver added.
