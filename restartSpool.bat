@ECHO OFF 
REM Title: restartSpool
REM Description: A script that will restart the spooler, default time is 5 seconds, unless specified by the user
REM Author: Gervel Giva

IF "%~1"=="" (
	SC STOP SPOOLER && TIMEOUT 5 && SC START SPOOLER
) ELSE (
	SC STOP SPOOLER && TIMEOUT %1 && SC START SPOOLER
)
