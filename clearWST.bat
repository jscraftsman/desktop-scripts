@ECHO OFF
::REM Title: clearWST
::REM Description: A batch file that will clear the logs of Wi-Fi Setup Tool
::REM Author: Gervel Giva

ECHO This script will clear the KDS Log...

:KDS_STOPPED
ECHO [1] Removing WiFiSetupTool.log
del "%temp%\Kyocera\WifiSetupTool\log\WiFiSetupTool.log"
IF ERRORLEVEL 1 (
  ECHO [2] Failed to remove WiFiSetupTool.log...
) ELSE (
  ECHO [2] WiFiSetupTool.log cleared...
)

ECHO [3] Removing kmnv.log
del "%temp%\Kyocera\WifiSetupTool\log\kmnv.log"
IF ERRORLEVEL 1 (
  ECHO [4] Failed to remove kmnv.log...
) ELSE (
  ECHO [4] kmnv.log cleared...
)

