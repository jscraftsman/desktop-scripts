@ECHO OFF 
REM Title: tWST
REM Description: Transfer DLL and EXE files of WiFi Setup Tool
REM Author: Gervel Giva

SET BUILD_PATH="\\KDDPPC340\Installers\WifiSetup"
SET TARGET_PATH="%homepath%\Desktop\WifiSetup"

XCOPY /Y %BUILD_PATH%\HelperClasses.dll %TARGET_PATH%\HelperClasses.dll
XCOPY /Y %BUILD_PATH%\WifiSetupToolModels.dll %TARGET_PATH%\WifiSetupToolModels.dll
XCOPY /Y %BUILD_PATH%\DeviceDiscoveryService.dll %TARGET_PATH%\DeviceDiscoveryService.dll
XCOPY /Y %BUILD_PATH%\DeviceWifiSettingsService.dll %TARGET_PATH%\DeviceWifiSettingsService.dll
XCOPY /Y %BUILD_PATH%\WifiSetupToolViewModels.dll %TARGET_PATH%\WifiSetupToolViewModels.dll
XCOPY /Y %BUILD_PATH%\WifiSetupToolViews.dll %TARGET_PATH%\WifiSetupToolViews.dll
XCOPY /Y %BUILD_PATH%\DeviceControllerService.dll %TARGET_PATH%\DeviceControllerService.dll
XCOPY /Y %BUILD_PATH%\WifiSetupToolMain.exe %TARGET_PATH%\WifiSetupToolMain.exe

XCOPY /Y %BUILD_PATH%\log4net.dll %TARGET_PATH%\log4net.dll
XCOPY /Y %BUILD_PATH%\PasswordEncrypt.dll %TARGET_PATH%\PasswordEncrypt.dll
XCOPY /Y %BUILD_PATH%\LoggerService.dll %TARGET_PATH%\LoggerService.dll
XCOPY /Y %BUILD_PATH%\SharpSnmpLib.dll %TARGET_PATH%\SharpSnmpLib.dll
XCOPY /Y %BUILD_PATH%\SharpSnmpLib.Engine.dll %TARGET_PATH%\SharpSnmpLib.Engine.dll
XCOPY /Y %BUILD_PATH%\SharpSnmpLib.Mib.dll %TARGET_PATH%\SharpSnmpLib.Mib.dll
XCOPY /Y %BUILD_PATH%\Xceed.Wpf.Toolkit.dll %TARGET_PATH%\Xceed.Wpf.Toolkit.dll

XCOPY /Y %BUILD_PATH%\en\HelperClasses.resources.dll %TARGET_PATH%\Localization\en.dat

XCOPY /Y %BUILD_PATH%\WifiSetupToolMain.exe.config %TARGET_PATH%\WifiSetupToolMain.exe.config

