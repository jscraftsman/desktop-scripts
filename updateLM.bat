@ECHO OFF 
REM Title: updateLM
REM Description: A batch script that will update the LM in windows spooler that supports KDService 1.1
REM Author: Gervel Giva
REM Version: 1.0
REM TODO: check if DLL file exist, if exist, suppress output of xcopy

REM Update this base the LM that will be used.

SET LM_VERSION=20140822

ECHO.
ECHO ^> Update LM script starting...

:CHECK_LM
TASKLIST | FIND "spoolsv.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
	ECHO ^> Spooler is still running.. Stopping Spooler...
	TIMEOUT 3 > NUL
	SC STOP spooler > NUL
	GOTO CHECK_LM
) ELSE (
	GOTO UPDATE_32
)

:UPDATE_32
ECHO ^> Updating LM files...
XCOPY /Y \\10.191.21.45\"Installers\KDSERVICE - CLIENT SERVER BUILDS\LanguageMonitor1.1\%LM_VERSION%\KXPLM32.dll" "C:\Windows\System32\KXPLM32.dll"
IF %ERRORLEVEL% EQU 1 (
  ECHO ^> Failed to update LM [KXPLM32.dll]...
  GOTO CHECK_LM
) ELSE (
  echo ^> KXPLM32.dll updated...
  GOTO UPDATE_64
)

:UPDATE_64
XCOPY /Y \\10.191.21.45\"Installers\KDSERVICE - CLIENT SERVER BUILDS\LanguageMonitor1.1\%LM_VERSION%\KXPLM64.dll" "C:\Windows\System32\KXPLM64.dll"
IF ERRORLEVEL 1 (
  ECHO ^> Failed to update LM [KXPLM64.dll]...
  GOTO CHECK_LM
) ELSE (
  echo ^> KXPLM64.dll updated...
  GOTO :END_UPDATE
)

:END_UPDATE
ECHO ^> Restarting Spooler...
SC START spooler > NUL
IF %ERRORLEVEL% EQU 0 (
	ECHO ^> Spooler running...
) ELSE (
	ECHO ^> Failed to start spooler. Trying again...
	TIMEOUT 3 > NUL
	GOTO END_UPDATE
)